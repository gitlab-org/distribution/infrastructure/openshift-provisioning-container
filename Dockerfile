ARG UBI_VERSION=8.9
FROM registry.access.redhat.com/ubi8/ubi:${UBI_VERSION} as base

ARG KUBERNETES_VERSION="1.26.7"
ARG HELM_VERSION="3.9.4"

RUN dnf install -y jq wget curl bash unzip

# Helm
RUN curl --retry 6 -Ls "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar -xz -C /tmp/ \
    && chmod +x /tmp/linux-amd64/helm \
    && mv /tmp/linux-amd64/helm /usr/local/bin/helm

# Kubectl
# RUN curl -o /usr/loca/bin/kubectl -L "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
RUN curl -o /usr/local/bin/kubectl -L "https://dl.k8s.io/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl" \
    && chmod +x /usr/local/bin/kubectl

FROM registry.access.redhat.com/ubi8/ubi:${UBI_VERSION}

COPY --from=base / /
